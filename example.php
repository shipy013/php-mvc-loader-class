<?php

// Initialize class or use static methods!

$load = new Loader();

/**
 * Load specific user library class
 */

$load->library('my_lib');
 // or
Loader::library('my_lib');

/**
 * Load specific user controller class
 */

$load->controller('blog');
 // or
Loader::controller('blog');


/**
 * Load specific user model class
 */

$load->model('Model_blog');
 // or
Loader::model('Model_blog');

/**
 * Load specific config
 */

$load->config('database')
$load->config('route')
 // or
Loader::config('database');
 // Load more configs
$data = array('database','route','config','my_config');
$load->config('data');

// ADD NEW PATH FROM OTHER DIRECTORIES

// If u want do add new path and autoload all files from other dir, flow  // next steps.

// First add in __constructor new line like this.

spl_autoload_register(array($this,'my_new_directory'));

// After adding spl_autoload_register in __constructor u must create new // method with name "my_new_directory".

public function my_new_directory($class)
{
  if ($class) {
     set_include_path('app/my_dir_path');
     spl_autoload_extensions('.php');
     spl_autoload($class);
    }
}

// Dont forget to change path in index constants

?>