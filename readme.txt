Installation is weary simple. Folow next steps:
----------------------------------------------------------------
1. Initialize loader class.
   
$loader = new Loader();

Or u can use getInstanece() like this.

Loader::getInstance();

All class from libraries, controllers, models will be loaded. Just need create new instance for all classes. Dont need include or require function.

----------------------------------------------------------------
Methods:
----------------------------------------------------------------

* getInstance() = Singleton instance
* load_specific_library()  = Method for load specific library.
* initialize_class()  = Methot for initializing defined class
* user_library() = Load all user lib's from app/library directories.
* system_library() = Load all core library from /lib/ directory or system/ 
* user_model() = Load all user models from /app/models/
* user_controller() = Load all user controller from /app/controllers/
* controller() = Load specific controller from /app/controllers/
* model() = Load specific model from /app/models/
* library() = Load specific library from /app/models/
* core_library() = Load specific library from /lib/
* config() = Load config files from /app/config. Create dir if missing.
