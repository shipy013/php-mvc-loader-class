<?php 
// Report all errors except E_NOTICE
//error_reporting(E_ALL ^ E_NOTICE);

//PATHS

define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
define('LIBPATH', str_replace("\\", "/", 'lib'.'/'));
define('APPATH', str_replace("\\", "/", 'app'.'/'));
define('LOGPATH', str_replace("\\", "/", 'logs'.'/'));
define('INCPATH', str_replace("\\", "/", 'include'.'/'));
define('CONFIGPATH', str_replace("\\", "/", APPATH.'config'.'/'));

// MVC Path
define('CPATH', str_replace("\\", "/", APPATH.'controllers'.'/'));
define('VPATH', str_replace("\\", "/", APPATH.'views'.'/'));
define('MPATH', str_replace("\\", "/", APPATH.'models'.'/'));
define('LPATH', str_replace("\\", "/", APPATH.'library'.'/'));

// Extension
define('EXT','.php');

require_once LIBPATH . 'loader'.EXT;  

$load = new Loader();





 