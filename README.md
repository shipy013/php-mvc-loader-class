This class can automatically load MVC and library classes. It can register several automatic class loader functions to load different types of project classes from different directories.
Currently it can load class scripts for models, controller, user and system library classes. The respective directories are configured using constants.

## Methods 

   * getInstance()
   * load_specific_library()
   * initialize_class()
   * user_library()
   * system_library()
   * user_model()
   * user_controller()
   * controller()
   * model()
   * library()
   * core_library()
   * config()

### How to use

If your application has a separate file that contains information about paths or information to the database, you can freely add the reserved constants which will contain the names of your directories.

    MPATH – model directory path
    CPATH – controller directory path
    LPATH -library directory path
    LIBPATH – core library directory path
    CPATH – config directory path

Initialize Class

    // Initialize class or use static methods!
    $load = new Loader();
    // or
    Loader::[method()];
    
### Load specific user library

First what u need opet your favorite text editor(notepad++) and create new class file with name ‘my_lib.php’ in root/mvc/application/library. After that you can load that library flowing this code:

    $load->library('my_lib');
    // or
    Loader::library('my_lib');

This code sample loads a library named “My_Lib“. Class with the name my_lib will be automatically loaded and initialized.
Load specific controller

If you have in your mvc application controller named Blog, you can load blog controller following this code.

    $load->controller('blog');
    // or
    Loader::controller('blog');

This method load specific controller, retrun object value and  expect one parameter [controller name].
Load specific model

This method load specific user model.

He retrurn object value and expect one parameter [model name].

    $load->model('blog_model');
    // or
    Loader::model('blog_model');
    Load specific config

This method load specific user config file. If u have config file in your application root/app/config/database.php u can load that file flowing next code:

This method retrurn string or array value and except one parametar [config name]	
    $load->config('database');
    // or
    Loader::config('database');

If u want to load multiple config files u can use array function like this:

    $config = array('database','route','config','my_config');
 
    $load->config($config);
 
    // or
 
    Loader::config($config);
    
###Add new phth

If u want do add new path and autoload all files from other dir, flow next steps:

First u must add this line code in __constructor
	
    spl_autoload_register(array($this,'my_new_directory'));

After adding spl_autoload_register function u must create new methot named ‘my_new_direcotry’).

    public function my_new_directory($class)
    {
    if ($class) {
    set_include_path('app/my_dir_path');
    spl_autoload_extensions('.php');
    spl_autoload($class);
    }
    }